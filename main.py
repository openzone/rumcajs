class Auto():

    def __init__(self, spotreba, hmotnost, nadrz):
        "kód, který se spustí, když vzniká nový objekt"
        self.spotreba = spotreba
        self.hmotnost = hmotnost
        self.nadrz = nadrz
        self.vnadrzi = 0
        self.najeto = 0

    def natankuj(self, mnozstvi):
        vysledek = self.vnadrzi + mnozstvi
        if vysledek <= self.nadrz:
            self.vnadrzi += mnozstvi
            return vysledek
        else:
            return None

    def jed(self, vzdalenost):
        vnadrzi = self.vnadrzi - self.spotreba/100 * vzdalenost
        if vnadrzi > 0:
            self.vnadrzi -= self.spotreba/100 * vzdalenost
            self.najeto += vzdalenost
            self.vnadrzi = 0

            return None

    def stav(self):
        print('nadrz:', self.vnadrzi)
        print('najeto:', self.najeto)


class Nakladak(Auto):

    def __init__(self, spotreba, hmotnost, nadrz, nostnost):
        super().__init__(spotreba, hmotnost, nadrz)
        self.nosnost = nostnost
        self.nalozeno = 0

    def stav(self):
        super().stav()
        print('Nalozeno', self.nalozeno)

    def naloz(self):
        pass

    def vyloz(self):
        pass


cerveny = Auto(8, 759, 40)
zeleny = Auto(6.8, 497, 30)

modry = Nakladak(6.2, 650, 35, 5000)

cerveny.natankuj(20)
cerveny.stav()
cerveny.jed(30)
cerveny.stav()

print('------------------')

modry.natankuj(50)
modry.stav()
modry.jed(10)
modry.stav()
