class Auto:
    nadrz = 0
    najeto = 0

    def __init__(self, pznacka, spotreba):
        self.pznacka = pznacka
        self.spotreba = spotreba

    def ukaz(self):
        print("znacka:", self.pznacka)
        print("spotreba:", self.spotreba)
        print("nadrz:", self.nadrz)
        print("najeto:", self.najeto)

    def tankuj(self, benzin):
        self.nadrz = self.nadrz + benzin

    def jed(self, km):
        jednotka = self.spotreba / 100
        ujeto = 0
        while self.nadrz > jednotka and ujeto < km:
            self.nadrz = self.nadrz - jednotka
            self.najeto = self.najeto + 1
            ujeto += 1
        return ujeto


class Nakladak(Auto):
    def __init__(self, pznacka, spotreba, nosnost):
        super().__init__(pznacka, spotreba)
        self.nosnost = nosnost

    def ukaz(self):
        super().ukaz()
        print("nostnot:", self.nosnost)


cerveny = Nakladak("BCR-8987", 8, 1000)
modry = Auto("BZE-8877", 6.5)

modry.tankuj(40)
print(modry.jed(200))
print(modry.jed(144))
print(modry.jed(150))
print(modry.jed(777))

modry.ukaz()
